# Slurm at IFB

## The Partitions

| Partitions | Description | Default |
| -- | --:|:--:|
| fast | job <= 24 hours | X |
| long | job <= 30 days |  |
| bigmem | job <= 60 days - on demand |  |
| training | on demand |  |

## The default values
| Param | Default value |
| -- | --:|
| --mem | 2GB |
| --cpus | 1 |

## IFB Core Cluster Computing nodes

(Last update: 30/07/2020)

| Nbr | CPU (Hyper-Thread)| RAM (GB )|
| --:| --:| --:|
| 12 | 30 | 128 |
| 2 | 38 | 258 |
| 66 | 54 | 258 |
| 1 | 124 | 3096 |